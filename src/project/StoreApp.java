package project;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreApp {

	
	/*Main method. It has minimal code.
	 * Creates ArrayList
	 * Starts Scanner
	 * Reads the file, calls the menu method.
	 * Prints out one welcome and one goodbye message
	 * */
	public static void main(String[] args) {
		//initiate scanner for user input
		Scanner input = new Scanner(System.in); 
		
		//create an ArrayList of type Product 
		ArrayList<Product> products = new ArrayList<Product>(); 
		
		// Load file (if exists)
		products = readFile(products);
		
		//Welcome message
		System.out.println("Welcome to Angela's Store." + "\n");
		
		// Menu
		products= menu(input, products);
		
		//Close Scanner
		input.close();
		
		//Exit message
		System.out.println("Thank you for shopping with at Angela's!");
	}
	
	
	/* This method reads from the stock.csv file.
	 * It stores instances of product appropriately in the class where
	 * they belong (regular product, produce, or age restricted)
	 * */
	public static ArrayList<Product> readFile(ArrayList<Product> products) {
		int count = 0;
		
		try {	
			//Reads products from the input file 
			File f = new File("src/project/stock.csv");
			Scanner input = new Scanner(f);
		
			
			while (input.hasNextLine()) {
			try {	
				String line = input.nextLine();
				String[] values = line.split(",");
				
				if (values.length == 2) { //regular product
					Product prod = new Product(values[0], Double.parseDouble(values[1]));
					products.add(prod);
					count++;
				}
				if ((values.length == 3) && (values[2].length() == 2)) { //age restricted product
					AgeRestrictedProduct a = new AgeRestrictedProduct(values[0], Double.parseDouble(values[1]), Integer.parseInt(values[2]));
					products.add(a);
					count++;
				}
				if ((values.length == 3) && (values[2].length() > 2)) { //produce product
					ProduceProduct pp = new ProduceProduct(values[0], Double.parseDouble(values[1]), values[2]);
					products.add(pp);
					count++;
				}				
			
			} catch (Exception e) {
				System.out.println("error");
				
			}
		} 
			input.close();
		} catch (FileNotFoundException fnf) {
			System.out.println("error in retrieving the file");
		}
		
		return products;
		
		}

	
	/* Menu method. 
	 * Presents main possible actions in the store
	 * Each action is implemented by calling a specific method
	 */
	public static ArrayList<Product> menu (Scanner input, ArrayList<Product> products) {
		boolean done= false;
		
		do {
			System.out.println("1. View all available products");
			System.out.println("2. Add a new product");
			System.out.println("3. Sell a product");
			System.out.println("4. Modify an existing product");
			System.out.println("5. Search for a product");
			System.out.println("6. Save current available stock");
			System.out.println("7. Exit");
			System.out.println("\n" + "Choose an option: ");
			String choice = input.nextLine();
			
			switch(choice) {
			case "1": ///View all available products
				products = displayProducts(products, input); 
				break;
			case "2": ///Add a new product
				products = addNewProduct(products, input); 
				break;
			case "3": //Sell a  product
					products = sellProduct(products, input);
				break;
			case "4": //Modify existing product
				products = modifyProduct(products, input); 
				break;
			case "5": //Search product
				products = searchProduct(products, input); 
				break;
			case "6": //Save current stock
				products = saveStock(products, input); 
				break;
			case "7": //Exit
				done = true;
				break;
			default:
				System.out.println("\n" + "This is not an option. Choose again" + "\n");
			}
		} while(!done);
		
		return products;
	}
	
	
	/*This method displays products (option 1 menu)
	 * It prints out all the products present in the ArrayList
	 * Uses the toString method in Product class and subclasses
	 */
	public static ArrayList<Product> displayProducts(ArrayList<Product> products, Scanner input) {
		// Iterates through ArrayList<Product> and prints all the products info.
		for(int i =0; i<products.size(); i++) {
			System.out.println(products.get(i).toString() + "\n");
		}
		return products;
	}
	
	
	/*This method allows user to add a new product to the ArrayList and the file.
	 * The method asks for all the products info
	 * implements try/catch to avoid quitting with error in case user input doesn't match what is expected
	 * finally prints out the product ArrayList showing user product has been successfully added
	 */
	public static ArrayList<Product> addNewProduct(ArrayList<Product> products, Scanner input) {
		try {
			FileWriter f = new FileWriter("src/project/stock.csv", true);
		
			System.out.println("Product name: ");
			String name = input.nextLine();
			
			double price = 0;
			System.out.println("Product price (in numbers): ");
			try { //makes sure price is in numbers so program doesn't end in error
			price = Double.parseDouble(input.nextLine());
			} catch (Exception e) { //tells user why price wasn't added
				System.out.println("Try again. Make sure your price only includes numerals" + "\n");
				try { //try to ask for price again
					System.out.println("Product price (in numbers): ");
					price = Double.parseDouble(input.nextLine());
				} catch (Exception x) {
					//after a second wrong price user is redirected to menu
					System.out.println("That's not a valid price. Returning to main menu" + "\n");
					menu(input, products);
				}
			}
		
			
			System.out.println("Does your product have an expiration date [y/n]: ");	
			String choice = input.nextLine();
			
			if (choice.equals("y") || choice.equals("yes") ) {
				System.out.println("Expiration month [01-12]: ");
				int month = Integer.parseInt(input.nextLine());
				System.out.println("Expiration day [01-31]: ");
				int day = Integer.parseInt(input.nextLine());
				System.out.println("Expiration year: ");
				int year = Integer.parseInt(input.nextLine());
				String expiration = month + "/" + day + "/" + year; 
				ProduceProduct pp = new ProduceProduct(name, price, expiration );
				
				//checks if product has expired, preventing user from adding it if that's the case
				if (pp.hasExpired(month, day, year)) {
					System.out.println("Sorry, it looks like the product you're trying to add has already expired" + "\n");
					menu(input, products);
				} else {
					products.add(pp);
					f.write(pp.getName() + "," + pp.getPrice() + "," + expiration + "\n");
					f.flush();
					f.close();
					System.out.println("This is the product you have added: " + "\n" + pp.toString());
				}
				
			} else if (choice.equals("n") || choice.equals("no")) {
				System.out.println("Does your product have an age restriction [y/n]: ");	
				String secondchoice = input.nextLine();
				if (secondchoice.equals("n") || secondchoice.equals("no")) /*regular product*/{
					Product prod = new Product(name, price);
					products.add(prod);
					f.write(prod.getName() + "," + prod.getPrice() + "\n");
					f.flush();
					f.close();
					System.out.println("This is the product you have added: " + "\n" + prod.toString());
				} else if (secondchoice.equals("y") || secondchoice.equals("yes") ) /*age restricted*/ {
					System.out.println("Customer needs to be over 18 or 21 years old? Type your age restriction in numbers ");
					int agechoice = 0;
					try { //makes sure age is in numbers so program doesn't end in error
						agechoice = Integer.parseInt(input.nextLine());
					} catch (Exception e) { //tells user why price wasn't added
					System.out.println("Try again. Make sure your age choice only includes numerals" + "\n");
						try { //try to ask for age choice again
							System.out.println("Age restriction (in numbers) [18/21]: ");
							price = Double.parseDouble(input.nextLine());
						} catch (Exception x) {
							//after a second wrong age restriction user is redirected to menu
							System.out.println("That's not a age restriction. Returning to main menu" + "\n");
							menu(input, products);
						}
					} 
							
				if ((Integer.compare(agechoice, 18))== 0) {
					AgeRestrictedProduct a = new AgeRestrictedProduct(name, price, agechoice);
					products.add(a);
					f.write(a.getName() + "," + a.getPrice() + "," + agechoice + "\n");
					f.flush();
					f.close();
					System.out.println("This is the product you have added: "+ "\n"  + a.toString());
				} else if ((Integer.compare(agechoice, 21))== 0) {
						AgeRestrictedProduct a = new AgeRestrictedProduct(name, price, agechoice);
						products.add(a);
						f.write(a.getName() + "," + a.getPrice() + "," + agechoice + "\n");
						f.flush();
						f.close();
						System.out.println("This is the product you have added: "+ "\n"  + a.toString());
				} else { //if agechoice = any number that is not 18 or 21
					System.out.println("This is not a valid age restriction.");
				}
			} else {
				System.out.println("This is not a valid choice");
			}	
	
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	return products;
	}


	/*This method allows user to sell a product
	 * User is prompted for product name. 
	 * User is prompted to add products to a cart
	 * User is asked to confirm they want to sell what is in the cart
	 * Finally, products sold are removed from ArrayList
	 */
	public static ArrayList<Product> sellProduct(ArrayList<Product> products, Scanner input) {
		//creates a new ArrayList of type Product to contain items user wants to add to their cart
		ArrayList<Product> cart = new ArrayList<Product>();
		
		//searches for product by name 
		System.out.println("Search product by name:");
		String name = input.nextLine();
		int count = 0;
		
		// Iterates through ArrayList<Product> and prints all the products info with matching name
		try {
		for(int i =0; i<products.size(); i++) {
			if (products.get(i).getName().equals(name)) {
				System.out.println("Results for "+ " '" + name + "':");
				System.out.println(products.get(i).toString());
				System.out.println("Do you want to add "+ products.get(i).getName() + " to your cart? [y/n]" );
				String choice = input.nextLine();
					if (choice.equals("y") || choice.equals("yes") )  {
						cart.add(products.get(i));
						System.out.println("Added to your cart" + "\n" + products.get(i) + "\n");
						count++;
					} else if(choice.equals("n") || choice.equals("no")) {
						System.out.println("This product was not added to your cart" + "\n");
						count++;
					} else {
						System.out.println("This is not a valid choice");
						count++;
					}
				}
			}  if (count == 0) { //if there is no result matching user-provided name
				System.out.println("Results for "+ " '" + name + "':");
				System.out.println("No products found with this name");
				return products;
			}
			
			//prompts user to confirm they want to sell items in their cart
			System.out.println("Your cart" + "\n" + cart + "\n");
			System.out.println("Confirm you want to sell the products in your cart? [y/n]");
			String secondchoice = input.nextLine();
			
			for(int y =0; y<cart.size(); y++) {
				if (secondchoice.equals("y") || secondchoice.equals("yes") ) {
					for(int x =0; x<products.size(); x++) {
						if (cart.get(y).toString().equals(products.get(x).toString())) {
							Product p = null;
							System.out.println(cart.get(y) + " was sold");
							p = products.get(x);
							products.remove(x); //removes item from the store
						}
					}
							
				} else if(secondchoice.equals("n") || secondchoice.equals("no")) {
					System.out.println("Checkout aborted");
				} else {
					System.out.println("This is not a valid choice");
				}
			}
		} catch (Exception e) {
			System.out.println("error");
		}
	
		//clears cart
		cart.clear();
		return products;
}
	
	
	/*This method allows user to modify existing products
	 * User searches a product by name.
	 * User can modify name and/or price
	 * User modifications are applied to every instance of the item (all items with specified name are modified)
	 */
	public static ArrayList<Product> modifyProduct(ArrayList<Product> products, Scanner input) {
		System.out.println("Search product by name:");
		String name = input.nextLine();
		int count = 0;
		// Iterates through ArrayList<Product> and prints all the products info with matching name
		for(int i =0; i<products.size(); i++) {
			if (products.get(i).getName().equals(name)) {
				System.out.println("Results for "+ " '" + name + "'" );
				System.out.println(products.get(i).toString() + "\n" );
				count++;
			}
		}
		if (count == 0) {
			System.out.println("Results for "+ " '" + name + "':");
			System.out.println("No products found with this name" + "\n");
		} else {
			System.out.println("Do you want to modify this product's name? [y/n]");
			String choice = input.nextLine();
			if (choice.equals("y") || choice.equals("yes") ) {
				Product y = null;
				System.out.println("Insert new product name:");
				String newname = input.nextLine();
				for (int x =0; x<products.size(); x++) {
					if (products.get(x).getName().equals(name)) {
						products.get(x).setName(newname);
						y= products.get(x);
					}
				}
				System.out.println("All products have been modified as follows: " + "\n" + y + "\n"); //all instance of product with user-provided name are modified with new name
					
			} else if (choice.equals("n") || choice.equals("no") ) {
				System.out.println("Do you want to modify this product's price? [y/n]");
				String secondchoice = input.nextLine();
					if (secondchoice.equals("y") || secondchoice.equals("yes") ) {
						Product y = null;
						System.out.println("Insert new product price:");
						double newprice = Double.parseDouble(input.nextLine());
						for (int x =0; x<products.size(); x++) {
							if (products.get(x).getName().equals(name)) {
								products.get(x).setPrice(newprice);
								y= products.get(x);
							}
						}
						System.out.println("All products have been modified as follows: " + "\n" + y + "\n"); //all instance of product with user-provided name are modified with new price
					} else if (secondchoice.equals("n") || secondchoice.equals("no") ) {
						System.out.println("Nothing about this product was modified");
					} else {
						System.out.println("This is not a valid choice");
					}
				} else {
					System.out.println("This is nt a valid choice");
				}
			}
		
		return products;
	}
	
	
	/*This method allows user to search for a product using its name
	 * Iterates through ArrayList<Product> and prints all the info of all products with matching name
	 */
	public static ArrayList<Product> searchProduct(ArrayList<Product> products, Scanner input) {
		// Iterates through ArrayList<Product> and prints all the products info with matching name
		System.out.println("Search product by name:");
		String name = input.nextLine();
		int count = 0;
		for(int i =0; i<products.size(); i++) {
			if (products.get(i).getName().equals(name)) {
				System.out.println("Result for "+ " '" + name + "'");
				System.out.println(products.get(i).toString() + "\n");
				count++;
			}
		}
		if (count == 0) {
			System.out.println("Results for "+ " '" + name + "':");
			System.out.println("No products found with this name" + "\n" );
		}

		return products;
	}
	
	/*This method allows user to save currently available stock into a new file.
	 * Uses a filewriter, overwriting the a designated user.csv file every time user saves stock
	 */
	public static ArrayList<Product> saveStock(ArrayList<Product> products, Scanner input) {
		
		System.out.println("Your name: ");
		String name = input.nextLine();
		File y = new File( "src/project/" + name + ".csv"); //new file to save current stock
		
		try {
			FileWriter f = new FileWriter(y);
		
			for(int i =0; i<products.size(); i++) { //write to user.csv all that is currently in stock as stored in ArrayList<Product> product 
				f.write(products.get(i).toString() + "\n"); 
			}
			f.flush();
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Current stock was saved in the " + name + ".csv file!");
		return products;
	}
}


