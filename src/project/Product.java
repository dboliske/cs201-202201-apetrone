//Angela Petrone 04/06/2022
//Superclass
//stores regular products with just name and price

package project;

public class Product {

	public String name;
	public double price;
	
	public Product() {
		name = "apple";
		price = 1.0;
	}
	
	public Product(String name, double price) {
		this.name = name;
		this.price= price;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price= price;
	}
	
	public String toString() {
		return "Product: " + name + ", price: " + price;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Product)) {
			return false;
		}
		
		return true;
	}
}
