//Angela Petrone 04/06/2022
//Extends Product
//stores produce products which other than just name and price
//also have an expiration date


package project;

public class ProduceProduct extends Product {
	
	public int expirationmonth;
	public int expirationday;
	public int expirationyear;
	public String expiration; 
	
	public ProduceProduct() {
		super();
		expirationmonth = 01;	
		expirationday = 22;
		expirationyear = 2023;
		expiration = expirationmonth + "/" + expirationday + "/" + expirationyear;
	}
	
	public ProduceProduct(String name, double price, int expirationmonth, int expirationday, int expirationyear, String expiration) {
		super(name, price);
		this.expirationmonth = expirationmonth;
		setExpirationmonth(expirationmonth);
		this.expirationday = expirationday;
		setExpirationday(expirationday);
		this.expirationyear = expirationyear;
		setExpirationyear(expirationyear);
		this.expiration = expiration;
	}
	
	public ProduceProduct(String name, double price, String expiration) {
		super(name, price);
		this.expiration = expiration;
	}
	
	public int getExpirationmonth() {
		return expirationmonth;
	}

	public void setExpirationmonth(int expirationmonth) {
		expirationmonth = String.valueOf(expirationmonth).length();
		if (expirationmonth == 2){
		this.expirationmonth = expirationmonth;
		}
	}

	public int getExpirationday() {
		return expirationday;
	}

	public void setExpirationday(int expirationday) {
		expirationday = String.valueOf(expirationday).length();
		if ((expirationday == 1)||(expirationday == 2)){
		this.expirationday = expirationday;
		}
	}

	public int getExpirationyear() {
		return expirationyear;
	}

	public void setExpirationyear(int expirationyear) {
		expirationyear = String.valueOf(expirationyear).length();
		if (expirationyear == 4) {
		this.expirationyear = expirationyear;
		}
	}

	public String getExpiration() {
		return expiration;
	}

	public void setExpiration(int expirationmonth, int expirationday, int expirationyear) {
		this.expiration = expirationmonth + "/" + expirationday + "/" + expirationyear;
	}
	
	public String toString() {
		return "Product: " + name + ", price: " + price + ", expires: " + expiration;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Product)) {
			return false;
		} else if (!(obj instanceof ProduceProduct)) {
			return false;
		}
		return true;
	}
	
	//custom method that checks if a product has expired
	public boolean hasExpired(int expirationmonth,int expirationday, int expirationyear) {
		if (expirationyear < 2022) {
			return true;
		} else if (expirationyear == 2022) {
			if (expirationmonth <= 05) {
				if (expirationday == 01) {
					return true;
				}
			}
		}
		
		return false;
	}

	
	
}
