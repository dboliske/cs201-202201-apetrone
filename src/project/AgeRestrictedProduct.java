//Angela Petrone 04/06/2022
//Extends Product
//stores products which other than just name and price
//also have an age restriction

package project;

public class AgeRestrictedProduct extends Product {
	
	public int age;
	
	public AgeRestrictedProduct() {
		super();
		age = 18;
	}
	
	public AgeRestrictedProduct(String name, double price, int age) {
		super(name, price);
		setAge(age);
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if ((age == 18)|| (age == 21)) {
		this.age = age;
		}
	}

	public String toString() {
		return "Product: " + name + ", price: " + price + ", age restriction: must be" + age + " or older";
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Product)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedProduct)) {
			return false;
		}
		return true;
	}

}
