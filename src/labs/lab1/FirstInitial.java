package labs.lab1;

import java.util.Scanner;

public class FirstInitial {

	public static void main(String[] args) {
	// create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
	// prompt the user for their first name 
		System.out.print("Your first name=");
		String firstname = input.nextLine();
		
	
	// output user's first initial
		System.out.print("Your first initial=" + firstname.charAt(0));
	
		input.close();
	}

}
