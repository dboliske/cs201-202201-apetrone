package labs.lab1;

import java.util.Scanner;

public class UserName {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
		Scanner input = new Scanner(System.in);

		// prompt user to type a name
		System.out.print("Type a name: ");
		
		// read in the next line of text typed by user
		String userinput = input.nextLine();
		
		input.close();

		// print out what the user entered
		System.out.println("Echo: " + userinput);
		
	}

}
