//* test table
//* measurements1: lenght=20' | width=40' | depth=60' | wood = 8800ft^2
//* measurements2: lenght=10' | width=20' | depth=30' | wood = 2200ft^2
//* measurements3: lenght=43' | width=27' | depth=89' | wood = 14782ft^2


package labs.lab1;

import java.util.Scanner;

public class WoodBox {

	public static void main(String[] args) {
	// create a Scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
	// prompt the user for the lenght of the box
		System.out.print("Lenght of the box=");
		double lenght = Double.parseDouble(input.nextLine());

	// prompt the user for the width of the box
		System.out.print("Width of the box=");
		double width = Double.parseDouble(input.nextLine());
		
	// prompt the user for the depth of the box
		System.out.print("Depth of the box=");
		double depth = Double.parseDouble(input.nextLine());
		
	// Calculate the amount of wood (square feet) needed to make the box.
		System.out.println("Amount of wood (square feet) needed to make the box = " + (2*((lenght*width)+(lenght*depth)+(width*depth))) + "ft^2 ");
		input.close();
		
	}

}
