//* test table
//* measurements1: inches= 62.8  | cm= 159.5
//* measurements2: inches= 1  | cm= 2.54
//* measurements3: inches=110  | cm= 279.4

package labs.lab1;

import java.util.Scanner;

public class InchToCm {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
			Scanner input = new Scanner(System.in);
			
		// prompt the user for their height in inches
			System.out.print("Your height in inches=");
			double Dinchesheight = Double.parseDouble(input.nextLine());		
				
		// perform the calculation: You height in centimeters	
			System.out.println("You height in centimeters = " + (Dinchesheight*2.54) + "cm");

			input.close();
	}

}
