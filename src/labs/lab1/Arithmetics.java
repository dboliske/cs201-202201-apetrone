package labs.lab1;

import java.util.Scanner;

public class Arithmetics {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
			Scanner input = new Scanner(System.in);
		
		// prompt the user for their age
			System.out.print("Your age=");
			double myage = Double.parseDouble(input.nextLine());

		// prompt the user for their father's age
			System.out.print("Your father's age=");
			double fatherage = Double.parseDouble(input.nextLine());
			
		// perform the calculation: Your age subtracted from your father's age		
			System.out.println("Your father's age - your age = " + (fatherage-myage));
		
		// prompt the user for their birth year
			System.out.print("Your birth year=");
			double birthyear = Double.parseDouble(input.nextLine());	
	
		// perform the calculation: Your birth year multiplied by 2	
			System.out.println("Your birth year multiplied by 2 = " + (birthyear*2));
	
		// prompt the user for their height in inches
			System.out.print("Your height in inches=");
			double Dinchesheight = Double.parseDouble(input.nextLine());	

		// perform the calculation: You height in centimeters	
			System.out.println("You height in centimeters = " + (Dinchesheight*2.54));
			
		//typecast the inches height (previously stored as a double) to an integer
			int Iinchesheight = (int) Dinchesheight;
					
		// perform the calculation: You height in feet and inches			
			System.out.println("You height in feet and inches = " +(Iinchesheight/12) + "' and " +(Iinchesheight%12) +"''.");
			input.close();

	}

}
