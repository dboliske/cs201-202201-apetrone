//* test table
//* low temperatures: 32F = 0°   | 14F = -10° | -22F = -20°
//* medium temperatures: 50F = 10°  | 60F = 15.5°  | 73F = 22.7°
//* high temperatures: 78F = 25.5°  | 90F = 32.2° |  150F = 65.5°


package labs.lab1;

import java.util.Scanner;

public class Temperatures {

	public static void main(String[] args) {
	// create a Scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
	// prompt the user a temperature in Fahrenheit
		System.out.print("Temperature in Fahrenheit= ");
		double fahrenheit = Double.parseDouble(input.nextLine());
		
	// convert the Fahrenheit to Celsius and display the result
		System.out.println("Your temperature in Celsius = " + ((fahrenheit-32)*5/9) + " Celsius");
	
	// prompt the user for a temperature in Celsius 
		System.out.print("Temperature in Celsius= " );
		double celsius = Double.parseDouble(input.nextLine());
		
	// convert the Celsius to Fahrenheit and display the result
		System.out.println("Your temperature in Fahrenheit = " + ((celsius*9/5)+32) + " Fahrenheit");
		
	input.close();
	
	}

}

//* I am satisfied with my program as it works as expected. I tested the program with all
//* the temperatures in my test table and it worked with no problems.

