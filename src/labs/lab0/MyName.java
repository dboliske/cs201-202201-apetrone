//a Java program that will output my name and birthdate
package labs.lab0;

public class MyName {

	public static void main(String[] args) {
		//declare variables for name and birthdate
		String name = "Angela Petrone";
		String birthdate = "Apr. 6 2002";
		
		//output my name and birthdate
		System.out.println("My name is " + name + " and my birthdate is " + birthdate + ".");
	}

}