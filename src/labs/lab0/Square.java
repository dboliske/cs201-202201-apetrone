/* I will use the character A to make my square
 * Line one: output three A
 * Line two: output three A
 * Line three: output three A
 * I will print three consecutive lines containing three A each
 * My square will be a 3x3 square, filled, made with As
 */

package labs.lab0;

public class Square {

	
	public static void main(String[] args) {
		//declare a variable containing the A forming square sides
		String letter = "AAA";
		
		//output 
		System.out.println(letter);
		System.out.println(letter);
		System.out.println(letter);
	}

}

/* I got the desired result, as the output of my code corresponds 
 * to the process I described at the top of the program, resulting in
 * a 3x3 square with sides formed by the letter A
 */
