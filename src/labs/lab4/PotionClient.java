package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		
		//instance of Potion using default constructor
		Potion p1 = new Potion();
		//instance of Potion using non-default constructor
		Potion p2 = new Potion ("Giuseppe", 0);
		
		//Display the values of each object by calling the toString method.
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		
	}

}
