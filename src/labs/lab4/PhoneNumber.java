package labs.lab4;

public class PhoneNumber {
	
	//Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
	private String countryCode;
	private String areaCode;
	private String number;
	
	//default constructor
	public PhoneNumber() {
		this.countryCode = "1";
		this.areaCode= "224";
		this.number= "1234567";
	}
	
	//non-default constructor
	public PhoneNumber (String cCode, String aCode, String number) {
		this.countryCode = cCode;
		this.areaCode = aCode;
		this.number= number;
	}
	
	//mutator method for cCode
	public void setCountryCode(String cCode) {
		this.countryCode = cCode;
	}
	
	//mutator method for aCode
	public void setAreaCode(String aCode) {
		this.areaCode = aCode;
	}
	
	//mutator method for number
	public void setNumber(String number) {
		this.number = number;
	}
	
	//accessor method for cCode
	public String getCountryCode() {
		return countryCode;
	}
	
	//accessor method for aCode
	public String getAreaCode() {
		return areaCode;
	}
	
	//accessor method for number
	public String getNumber() {
		return number;
	}
	
	//method that will return the entire phone number as a single string
	public String toString () {
		return countryCode + areaCode + number;
	}
	
	//method that will return true if the areaCode is 3 characters long.
	public boolean validAreaCode(String aCode) {
		if (aCode.length() > 3) {
			return true;
		}
		return false;
		
	}
	
	//method that will return true if the number is 7 characters long.
	public boolean validNumber(String number) {
		if (number.length() > 7) {
			return true;
		}
		return false;
		
	}
	
	//method that will compare this instance to another PhoneNumber
	public boolean equals (PhoneNumber d) {
		if (this.areaCode != d.getAreaCode()) {
			return false;
		} else if (this.countryCode != d.getCountryCode()) {
			return false;
		} else if (this.number != d.getNumber()) {
			return false;
		}
		
		return true;
	}
}
