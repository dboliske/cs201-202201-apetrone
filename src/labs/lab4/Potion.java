package labs.lab4;

public class Potion {
	
	//Create two instance variables, name (String) and strength (double).
	private String name;
	private double strenght;
	
	//default constructor
	public Potion(){
		name = "Angela";
		strenght = 10.0;
	}
	
	//non-default constructor
	public Potion(String name, double strenght) {
		this.name = name;
		this.strenght= strenght;
		setStrenght(strenght);
		
	}
	
	//accessor method for name
	public String getName() {
		return name;
	}
	
	//accessor method for strenght
	public double getStrenght () {
		return strenght;
	}
	
	//mutator method for name
	public void setName (String name) {
		this.name= name;
	}
	
	//mutator method for strenght
	public void setStrenght (double strenght) {
		if (this.strenght >= 0) {
			this.strenght = strenght;
		}
	}
	
	//a method that will return true if the strength is between 0 and 10.
	public boolean validStrenght(double strenght) {
		if ((0 < this.strenght) && (this.strenght > 10)){
			return true;
		} 
		return false;
	}
	
	//a method that will compare this instance to another Potion
	public boolean equals(Potion d) {
		if (this.name != d.getName()) {
			return false;
		} else if (this.strenght != d.getStrenght()) {
			return false;
		}
		
		return true;
	}
	
	// a method that will return the entire as a single string
	public String toString() {
		return "Your potion information: " + "name: " + name + ", strenght: " + strenght;
	}
	
}
