package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		
		//instance of PhoneNumber using default constructor
		PhoneNumber d1 = new PhoneNumber();
		//Display the values of the object by calling the toString method.
		System.out.println(d1.toString());
		
		//instance of PhoneNumber using non-default constructor
		PhoneNumber d2 = new PhoneNumber("1", "224", "1234967");
		//Display the values of the object by calling the toString method.
		System.out.println(d2.toString());

	}
	
}
