# Lab 4

## Total

30/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        8/8
  * Application Class   1/1
* Documentation         3/3

## Comments
Good work
In the non default constructors, it would be helpful to call the mutator methods.
Once you did that you do not need to do (this.lat = lat) for example