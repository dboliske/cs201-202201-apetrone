package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	public static int search(String[] array, String value, int start, int end) {
		int pos = -1;
		int middle = (start + end)/ 2;
		if (end >= start && start <= array.length - 1) {
			if (array[middle].equalsIgnoreCase(value)) {
				pos = middle;
				return pos;
			} else if (array[middle].compareToIgnoreCase(value) < 0) {
				return search(array, value, middle + 1, end);
			} else {
				return search(array, value, start, middle-1);
			}
		}
		return pos;
	}
	
	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String value = input.nextLine();
		int index = search(lang, value, 0, (lang.length - 1));
		if (index != -1) {
			System.out.println(value + " found at index " + index + ".");
		} else {
			System.out.println(value + " not found.");
		}

		input.close();
	}

}
