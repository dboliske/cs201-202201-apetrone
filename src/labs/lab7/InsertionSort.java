package labs.lab7;

public class InsertionSort {
	
	public static String[] sort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] words = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		words = sort(words);
		
		for (String w : words) {
			System.out.print(w + " ");
		}
	}

}
