package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;


public class DeliQueue {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in); //read user input
		ArrayList<String> data = new ArrayList<String>(); //create an arraylist to store customers
		data = menu(input, data); ///call menu method

		input.close(); //close scanner
		
		System.out.println("Goodbye");
	}


	public static ArrayList<String> menu(Scanner input, ArrayList<String> data) {
		
		//flag variable to control do-while loop
		boolean done = false;
		
		String[] options = {"Add customer to queue", "Help customer", "Exit"};
		
		//do-while loop to go through menu options
		do {
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1) + ". " + options[i]);
			}
			System.out.print("Choice: ");
			String choice = input.nextLine(); //store user choice
			
			switch (choice) {
			case "1": //add a customer
				data = addCustomers(input, data);
				break;
			case "2": //help a customer
				data = helpCustomers(input, data);
				break;
			case "3": //exit
				done = true;
				break;
			default:
				System.out.println("That is not a valid choice. Choose again.");
				break;	

			}
			
		} while (!done); //end loop
		
		return data;
	
	}
		
		
		
	public static ArrayList<String> addCustomers(Scanner input, ArrayList<String> data) {
		String c;
		System.out.println("Customer Name: "); //prompt for customer name
		String name = input.nextLine(); //store customer name
		c = new String(name);
	
		data.add(c); //add customer to queue
		
		int position = data.indexOf(c); //get new customer position in the queue
		
		//print out new customer position in the queue
		System.out.println(c + " is in position " + ( position + 1));
		
		return data;
	}
	
	
	
	public static ArrayList<String> helpCustomers(Scanner input, ArrayList<String> data) {
		
		try {
		String element = data.get(0); //get first customer in the queue (arraylist position 0)
		data.remove(0); //remove first customer in the queue
		//print out the removed customer's name
		System.out.println("Thank you for your help. " + element + " was removed from the queue.");
		} catch (Exception e) { //avoid program crash in case all customers have been helped
			System.out.println("There's no customers in need of help at this time.");
		}
		
		return data;
	
	}
}
		
		
		
		
		
		
