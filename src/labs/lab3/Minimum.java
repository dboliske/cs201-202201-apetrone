package labs.lab3;

public class Minimum {

	public static void main(String[] args) {
		
		//create an array of type int with the values provided in the readme file
		int [] numbers = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		//create a variable to store the minimum value in the array
		int minimum = numbers[0];
		
		for (int i=0; i<numbers.length; i++) {
			if (numbers[i] < minimum) {
				minimum = numbers[i];
				//check the entire array for the lowest value and update minimum variable accordingly
			}
		}
		
		System.out.println("The minimum number is:"+ minimum); //print out the minimum
	}

}
