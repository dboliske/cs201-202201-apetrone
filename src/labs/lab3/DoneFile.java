package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DoneFile {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //create scanner for user input
		
		double [] numbers = new double[0];
		boolean done = false; //create a flag variable to control loop
		
		while(!done) {
			System.out.println("1. Enter a number or type Done to finish");
			String choice = input.nextLine();

			if (choice.equalsIgnoreCase("Done")) { //use an if/else loop to specify different behaviors based on user input
				done = true;
				}
			else {
				double x = Double.parseDouble(choice);
				double [] newnumbers = new double[numbers.length+1];
				for (int i=0; i<numbers.length; i++) {
					newnumbers[i]= numbers[i];
					}
				numbers = newnumbers;
				numbers[numbers.length-1] = x;
			} //increase the length of the "numbers" array
		}
		
		try { //initiate a try catch block to handle possible exceptions related to FileWriter
			System.out.println("Enter a file name so that your values can be saved to the file: "); //prompt user for file name
			String line = input.nextLine(); //store user chosen file name
			FileWriter f = new FileWriter("src/labs/lab3/"+ line +".txt"); //create a file with user chosen name
			for (int i=0; i<numbers.length; i++) { 
				f.write(numbers[i] + "\n");
			} //use a loop to write all the numbers the user inserted in a new file called with their chosen name
				f.flush();
				f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} //use getMessage method and print all to console in case IOException happens

		
		System.out.println("Bye Bye");
		
		
		input.close(); //close Scanner
		 

	}

}
