package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ClassAverage {

	public static void main(String[] args) throws IOException {
		File f = new File ("src/labs/lab3/grades.csv"); //create a File variable specifying path to a specific file to read from
		
		Scanner input = new Scanner(f); //create a scanner to read from file
		int count = 0; //create a variable to count the number of grades
		double gradesum = 0; //create a variable to store the sum of grades
		
		while (input.hasNextLine()) {
			String line = input.nextLine();
		    String[] grade = line.split(","); 
		   // System.out.println(grade[1]);
		    double gradeint = Double.parseDouble(grade[1]); //only store the second part of each line (position 1), containing the grades
		    gradesum = gradesum + gradeint; //add each grade to the sum of grades
		    count++; //increase the variable keeping track of how many grades there are
		}
		
		System.out.println("The class average is: " + gradesum/count); //print out class average 
		
		
		input.close();
	}

}
