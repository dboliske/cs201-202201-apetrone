package labs.lab5;

public class GeoLocation {
	
	//Create two instance variables for latitude and longitude
	private double lat;
	private double lng;
	
	//default constructor
	public GeoLocation() {
		this.lat= 0.0;
		this.lng= 0.0;		
	}
	
	//non-default constructor
	public GeoLocation (double lat, double lng) {
		this.lat = lat;
		setLat(lat);
		this.lng = lng;
		setLng(lng);
	}
	
	//mutator method for latitude
	public void setLat(double lat) {
		if ((lat>= -90.0) && (lat <= 90.0)) {
			this.lat=lat;
		}
	}
	
	//mutator method for longitude
	public void setLng(double lng) {
		if ((lng>= -180.0) && (lng <= 180.0)) {
			this.lng=lng;
		}
	}
	
	//accessor method for latitude
	public double getLat() {
		return lat;
	}

	//accessor method for longitude
	public double getLng() {
		return lng;
	}
	
	//method that will return true if the latitude is between -90 and +90
	public boolean validLat(double lat) {
		if ((-90.0 < lat) && (lat > 90.0)) {
			return true;
		}
		return false;
		
	}
	
	//method that will return true if the longitude is between -180 and +180.
	public boolean validLng(double lng) {
		if ((-180.0 < lng) && (lng > 180.0)) {
			return true;
		}
		return false;
		
	}
	
	//method that takes another GeoLocation and returns a double.
	public double calcDistance (GeoLocation d) {
		double distance = Math.sqrt(Math.pow(this.lat - d.lat, 2) + Math.pow(this.lng - d.lng, 2));
		return distance;
	}
	
	//method that takes a lng and lat and returns a double.
	public double calcDistance (double lat, double lng) {
		double distance = Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
		return distance;
	}
	
	//equals method (will compare this instance to another GeoLocation)
	public boolean equals(GeoLocation d) {
		if (this.lat != d.getLat()) {
			return false;
		} else if (this.lng != getLng()) {
			return false;
		} 
		
		return true;
	}

	//method that will return the location in the format "(lat, lng)"
	public String toString() {
		return "lat: " + lat + " - lng: " + lng;
	}

}