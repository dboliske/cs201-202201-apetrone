package labs.lab5;

public class CTAStation extends GeoLocation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation(){
		super();
		name = "Roosevelt";
		location= "Chicago";
		wheelchair = true;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location= location;
		this.wheelchair = wheelchair;
		this.open = open;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isOpen() {
		return open;
	}

	@Override
	public String toString() {
		return "CTAStation [name=" + name + ", location=" + location + ", wheelchair=" + wheelchair + ", open=" + open
			+ super.toString() + "]";
	}
	
	
	//equals method (will compare this instance to another CTAStation)
	public boolean equals(CTAStation c) {
		if (!super.equals(c)) {
			return false;
		} else if (this.name != getName()) {
			return false;
		} else if (this.name != getName()) {
			return false;
		} else if (this.location != getLocation()) {
			return false;
		} else if (this.wheelchair != hasWheelchair()) {
			return false;
		} else if (this.open != isOpen()) {
			return false;
		} 
		return true;
		
	}

	
	
}
