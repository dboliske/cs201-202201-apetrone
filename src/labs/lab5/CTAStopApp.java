package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
	

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// Load file (if exists)
		CTAStation[] data = readFile();
		
		// Menu
		data= menu(input, data);
		
		input.close();
		System.out.println("Goodbye!");
	}
	
	
	
	public static CTAStation[] readFile() {
		CTAStation[] stations = new CTAStation[34];
		int count = 0;
		
		try {	
			//Reads stations from the input file 
			File f = new File("src/labs/lab5/CTAStops.csv");
			Scanner input = new Scanner(f);
			String titles = input.nextLine();
			
			while (input.hasNextLine()) {

				String line = input.nextLine();
				String[] values = line.split(",");
				
				//store the data in an instance of CTAStation[].

				CTAStation c = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]),  Boolean.parseBoolean(values[5]));
				
				if (stations.length == count) {
					stations = resize(stations, stations.length*2);
				}
				
				stations[count] = c;
				count++;
			
				} 
		input.close();
		}catch (FileNotFoundException fnf) {
				// Do nothing.
		}catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		stations = resize(stations,count);
		
		return stations;
		
		
		}	
	
	
	public static CTAStation[] resize(CTAStation[] data, int size) {
		//resize CTAStation array
		CTAStation[] temp = new CTAStation[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}

	public static CTAStation[] menu (Scanner input, CTAStation[] data) {
		boolean done= false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			
			switch(choice) {
			case "1": //Display Station Names
				data = displayStationNames(data, input);
				break;
			case "2": //Display Stations with/without Wheelchair access
				data = displayByWheelchair(data, input);
				break;
			case "3": //Display Nearest Station
				data = displayNearest(data, input);
				break;
			case "4": //Exit
				done = true;
				break;
			default:
				System.out.println("That is not a valid choice");
			}
		} while(!done);
		
		return data;
	}
	
	
	
	public static CTAStation[] displayStationNames(CTAStation[] data, Scanner input) {
		// Iterates through CTAStation[] and prints the names of the stations.
		for(int i =0; i<data.length; i++) {
			System.out.println(data[i].getName());
		}
		return data;
	}
	
	
	public static CTAStation[] displayByWheelchair(CTAStation[] data, Scanner input) {
		
		//Prompts the user for accessibility ('y' or 'n')
		System.out.print("Station has wheelchair access (y/n): "); 
		boolean accessibility = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				accessibility = true;
				double accessiblestations = 0;
				//loop through stations array to get accessible stations
				for(int i =0; i<data.length; i++) {
					boolean stationaccess= data[i].hasWheelchair();
					if (stationaccess = accessibility) {
					System.out.println(data[i].getName());
					accessiblestations++;
					}
				}
				//Display a message if no stations are found
				if (accessiblestations == 0) {
					System.out.println("no station matches your accessibility requirement");
					}
				break;
			case "n":
			case "no":
				accessibility = false;
				double accessiblestat = 0;
				
				//loop through stations array to get not accessible stations
				for(int i =0; i<data.length; i++) { 
					boolean stationaccess= data[i].hasWheelchair();
					if (stationaccess = accessibility) {
					System.out.println(data[i].getName());
					accessiblestat++;
					}
				}	
				//Display a message if no stations are found
				if (accessiblestat == 0) { 
					System.out.println("no station matches your accessibility requirement");
					}
				break;
			default: //continues to prompt the user for 'y' or 'n' until one has been entered
				System.out.println("That is not yes or no. Returning to menu.");
				return data;
		}
		return data;
	}
	
	public static CTAStation[] displayNearest(CTAStation[] data, Scanner input) {
		//Prompts the user for a latitude and longitude
		System.out.println("Insert a Latitude: ");
		double userlat = Double.parseDouble(input.nextLine());
		System.out.println("Insert a Longitude: ");
		double userlng = Double.parseDouble(input.nextLine());
		
		Double[] distance = new Double[34];
		double minimum;
		int savei = 0;
		
		//Uses the values entered to iterate through the CTAStation[] to find the nearest station
		for(int i =0; i<data.length; i++) {
			distance[i] = data[i].calcDistance (userlat, userlng);
		}
		
		minimum= distance[0];
		for (int x=1; x< distance.length; x++) {
			if (distance[x] < minimum) {
				minimum = distance[x];
				//System.out.println(minimum);
				savei = x;
			} 
		}
		
		//displays nearest station to the console
		System.out.println(data[savei].getName());
		return data;
	}
	
}
