package labs.lab5;

public class GeoLocationClient {

	public static void main(String[] args) {
		
		//instance of GeoLocation using default constructor
		GeoLocation g1 = new GeoLocation();
		//instance of GeoLocation using non-default constructor
		GeoLocation g2 = new GeoLocation( -90.3, 180.0);
		
		//Display the values of each object by calling the accessor method.
		System.out.println(g1.getLat());
		System.out.println(g1.getLng());
		System.out.println(g2.getLat());
		System.out.println(g2.getLng());
	}

}