package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// create a Scanner for user input
			Scanner input = new Scanner(System.in);
			
		//declare a flag variable to control loop
			boolean done= false;
		
		do {
			System.out.println("1. Say Hello'");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication ");
			System.out.println("4. Exit ");
			System.out.print("Choice: ");
			String choice = input.nextLine(); // get user's choice
		
			switch (choice) {
			case "1":
				System.out.println("Hello"); //print Hello to the console
				break;
			case "2":
				System.out.print("Enter one number:");
				int numberone = Integer.parseInt(input.nextLine()); //store the first number
				System.out.print("Enter a second number:");
				int numbertwo = Integer.parseInt(input.nextLine());//store the second number
				System.out.println("The sum of your numbers is: " + (numberone + numbertwo)); //perform addition
				break;
			case "3":
				System.out.print("Enter one number:");
				int numone = Integer.parseInt(input.nextLine());//store the first number
				System.out.print("Enter a second number:");
				int numtwo = Integer.parseInt(input.nextLine());//store the second number
				System.out.println("The multiplication of your numbers is: " + (numone*numtwo)); //perform multiplication
				break;
			case "4":
				done= true; //modify the flag variable to end the loop
				break;	
			default:
				System.out.println("That is not a valid choice.");
			}
		
		} while (!done);
		
		input.close();
		System.out.print("Goodbye");
	}
}
