package labs.lab2;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		// create a Scanner for user input
			Scanner input = new Scanner(System.in);

		// define variables to store sum of grades and number of grades
			double gradenum = 0;
			double gradesum = 0;
	
			//declare a flag variable to control loop
			boolean done= false;
			
			do {
			System.out.println("1. Insert a grade'");
			System.out.println("2. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine(); // get user's choice
			int userchoice = Integer.parseInt(choice);
				
			if (userchoice == 1) {
				System.out.println("Insert your grade=");
					String grade = input.nextLine();
					double newgrade = Double.parseDouble(grade);
					gradenum++; //increase the number of grades
					gradesum = gradesum + newgrade; //increase the sum of grades
					}
				else if (userchoice == 2) {
					System.out.println("Your grades average is:" + gradesum/gradenum);
					done= true; //end the loop by changing the flag
				}
				else {
				System.out.println("That is not a valid choice.");
				}
			
			} while (!done);
			
			input.close();
			
			System.out.println("Goodbye"); //print a message after the end of the loop
		}
	}
