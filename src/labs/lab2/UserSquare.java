package labs.lab2;

import java.util.Scanner;

public class UserSquare {

	public static void main(String[] args) {
		// create a Scanner for user input
		Scanner input = new Scanner(System.in);

		// prompt the user
		System.out.print("Choose the size of your square=");
		int size = Integer.parseInt(input.nextLine()); //get user's chosen size
		
		for (int row=0; row<size; row++) {
			for (int col=1; col<size; col++) {
			System.out.print("* "); //print the vertical dimension of the square
			}
			System.out.println("*");//print the horizontal dimension of the square
		}
		
		input.close();
	}
}
	
