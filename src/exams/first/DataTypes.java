package exams.first;

import java.util.Scanner;

public class DataTypes {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
			Scanner input = new Scanner(System.in);
				
		// prompt the user for an integer
			System.out.print("Insert an integer=");
			int userchoice = Integer.parseInt(input.nextLine());

		// perform the calculation: user's integer + 65	and then turn the result into a character	
			userchoice= userchoice + 65;
			char x = (char) userchoice;
			
		//print character to the console
			System.out.println(x);
			
			input.close();
	}

}
