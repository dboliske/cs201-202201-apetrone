package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name= "My Doggo";
		age = 8;
		setAge(age);
	}
	
	public Pet(String name, int age) {
		this.name= name;
		this.age= age;
	}
	
	public void setName (String name) {
		this.name= name;
	}
	
	public void setAge (int age) {
		if (this.age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet d) {
		if (this.name != d.getName()) {
			return false;
		} else if (this.age != d.getAge()) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return "Your dog's name is " + name + " and he is " + age;
	}
}
