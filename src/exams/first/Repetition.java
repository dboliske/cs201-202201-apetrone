package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
			Scanner input = new Scanner(System.in);
								
		// prompt the user for an integer
			System.out.print("Insert an integer=");
			int y = Integer.parseInt(input.nextLine());

			for (int a=0; (y-1)>a; a++) {
				System.out.println("*");
				for (int b= a; (y-1)>b; b++) {
					System.out.print("* ");	
				}
			}
			
			input.close();
	}

}
