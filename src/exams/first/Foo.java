package exams.first;

import java.util.Scanner;

public class Foo {

	public static void main(String[] args) {
		// create a Scanner for reading in user input
			Scanner input = new Scanner(System.in);
						
		// prompt the user for an integer
			System.out.print("Insert an integer=");
			int x = Integer.parseInt(input.nextLine());

			if ((x%3 == 0) && (x%2 == 0)){
				System.out.print("foobar");
			} else if (x%2 == 0) {
				System.out.print("bar");
			} else if (x%3 == 0){
				System.out.print("foo");
			}
	
	input.close();
	}
}
