package exams.second;

public class Rectangle extends Polygon {
	public double width;
	public double height;
	
	public Rectangle() {
		width = 10;
		height = 31;
	}

	public Rectangle(String name, double width, double height) {
		super(name);
		setWidth(width);
		this.width = width;
		setHeight(height);
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if (width >= 1) {
			this.width = width;
		} else if (width <= 0) {
			this.width = 1;
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		if (height >= 1) {
			this.height = height;
		} else if (height <= 0) {
			this.height = 1;
		}
	}

	@Override
	public String toString() {
		return "Name= " + name + ", width= " + width + ", height= " + height;
	}
	
	@Override
	double Area() {
		System.out.print("Area= ");
		return height * width; 
	}

	@Override
	double Perimeter() {
		System.out.print("Perimeter= ");
		return (2.0 * (height + width));  
	}

}
