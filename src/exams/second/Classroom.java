package exams.second;

public class Classroom {


	protected String building;
	protected String roomNumber;
	public int seats;
	
		public Classroom() {
			building= "Stuart";
			roomNumber = "201";
			seats = 50;
		}
			
		public Classroom(String building, String roomNumber, int seats) {
			this.building= building;
			this.roomNumber= roomNumber;
			this.seats= seats;
			setSeats(seats);
		}

		public String getBuilding() {
			return building;
		}

		public void setBuilding(String building) {
			this.building = building;
		}

		public String getRoomNumber() {
			return roomNumber;
		}

		public void setRoomNumber(String roomNumber) {
			this.roomNumber = roomNumber;
		}

		public int getSeats() {
			return seats;
		}

		public void setSeats(int seats) {
			if (seats > 1) {
				this.seats = seats;
			}
		}
			
		public String toString() {
			return "Building " + building + ", room number: " + roomNumber + ", seats: " + seats ;
		}

		
}
