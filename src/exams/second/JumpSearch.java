package exams.second;

import java.util.Scanner;

public class JumpSearch {

	public static int search(Double[] array, double value, int step, int prev) {	
		
		while (array[Math.min(step, array.length)] < value) {
			try {
			step += (int)Math.sqrt(array.length);
			search(array, value, step, step);
			} catch (Exception e) {
				return -1;
			}
			
			if (prev <= array.length) {
				try {
					step += (int)Math.sqrt(array.length);
					search(array, value, step, step);
				} catch (Exception e) {
					return -1;
				}
			}
		}
		
		while (array[prev] < value) {
			return search(array, value, step, prev+1);
		}
		
		if (array[prev] == value) {
			return prev;
		}
		
		return -1;
	}

	public static void main(String[] args) {
		Double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search number: ");
		Double value = Double.parseDouble(input.nextLine());
		int index = search(numbers, value, (int)Math.sqrt(numbers.length), 0);
		if (index == -1) {
			System.out.println("-1");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}
}	


