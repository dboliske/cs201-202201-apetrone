package exams.second;

abstract class Polygon {
	
	protected String name;

	public Polygon() {
		name = "square";
	}
	
	public Polygon(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "Polygon name: " + name;
	}
	
	abstract double Area();
	
	abstract double Perimeter();
}
