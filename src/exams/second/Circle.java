package exams.second;

public class Circle extends Polygon {
	public double radius;
	
	public Circle() {
		radius = 10;
	}

	public Circle(String name, double radius) {
		super(name);
		setRadius(radius);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		if (radius >= 1) {
			this.radius = radius;
		} else if (radius <= 0) {
			this.radius = 1;
		}
	}

	@Override
	public String toString() {
		return "Name= " + name + ", radius= " + radius;
	}
	
	@Override
	double Area() {
		System.out.print("Area= ");
		return  Math.PI * radius * radius;
	}

	@Override
	double Perimeter() {
		System.out.print("Perimeter= ");
		return (2.0 * Math.PI * radius);
	}

}
