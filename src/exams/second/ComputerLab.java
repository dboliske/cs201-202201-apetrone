package exams.second;

public class ComputerLab extends Classroom {
	public boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}
	
	public ComputerLab (String building, String roomNumber, int seats, boolean computers) {
		super(building, roomNumber, seats);
		this.computers = computers;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public String toString() {
		return "Building " + building + ", room number: " + roomNumber + ", seats: " + seats + ", has computers: " + computers ;
	}
}
