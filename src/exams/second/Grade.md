# Final Exam

## Total

97.5/100

## Break Down

1. Inheritance/Polymorphism:    19.5/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                4.5/5
    - Methods:                  5/5
2. Abstract Classes:            18/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                4.5/5
    - Methods:                  3.5/5
3. ArrayLists:                  20/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  5/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:          10/10
    - Results:                  5/5
5. Searching Algorithms:       20/20
    - Compiles:                 5/5
    - Jump Search:             10/10
    - Results:                  5/5

## Comments

1.instance variables that come after a - sign should be private
2.the abstract methods should be public, - sign before the variables means private, the height,width and radius can have values between 0 and 1
3.Good
4.Good
5.Good
